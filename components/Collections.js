import React from 'react';
import axios from 'axios';
import  { useState,useEffect } from 'react';
import Cards from './Cards';
import { useCart } from 'react-use-cart';

function Collections() {
  const [data,setData] = useState([]);
  useEffect(() =>{
    const fetchData =async () =>{
      try{
        let response = await axios.get("http://localhost:3005/fetch")
        console.log(response.data)
        setData(response.data)  
      }
      catch(err){
      }
    };
    fetchData()
  },[]);
  const { addItem } = useCart();
  return (
    <div style={{paddingTop:80}}>
      <marquee><h6 style={{background:'goldenrod',textAlign:'center'}}>Today: Gold 24k-10g = RS. 62,800 | Today: Silver-10g = RS.800 | Today: Platinum-10g = RS. 33,800</h6></marquee>
        
        <div>
          <h1 className='text-info text-center text-black fs-10'>Top Collections</h1><span><ur/></span>
          <div className='container'>
          <div className='row mt-5'>{data.map((e)=>(
            // <img src={e.Img} alt=""  className='w-25  rounded-5'/>
            // <Cards data={e}/>
            <div className='col-12 col-sm-4 col-md-3 col-lg-3'>
             <img src={e.image} className='w-100 h-200 rounded-5 object-fit-cover' style={{height:'200px'}} />
             <div className='card-body'>
              <p className='text-black text-center fs-5' >{e.cardname}</p>
              <p className='text-bold fs-4 text-center text-black'>RS. {e.price}/-</p>
              {/* <p className='text-center text-white'>{props.data.Story}</p> */}
             <center><p className='btn btn-primary text-center' onClick={()=>addItem(e)}>Buy Now</p></center> 
              </div>      
      </div>
          ))} 
        </div>  
        </div>
        </div>

    </div>
  )
}

export default Collections