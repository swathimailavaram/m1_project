// import React from 'react'

// function Login() {
//   return (
//     <div>Login</div>
//   )
// }

// export default Login
import React, { useState } from 'react';
import axios from 'axios'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter,Form,FormGroup,Input,Label } from 'reactstrap';
import Registration from '../Registration';
import { useHistory, useNavigate } from 'react-router-dom';
import { useUser } from './UserContext';

function Login(args) {
  const [modal, setModal] = useState(false);
  const navigate = useNavigate();
  const { loginUser } = useUser();
  const toggle = (e) => {
    e.preventDefault()
    setModal(!modal);
  }
  const [formdata,setFormdata] = useState({
    email:'',
    password:''
  })
  const handleInput = (e) =>{
    const {name,value} = e.target
    setFormdata({
      ...formdata,
      [name]:value
    })
  }
  // const handleSubmit = async (e) =>{
  //   e.preventDefault();
  //   try{
  //     let  response = await axios.get(`http://localhost:3005/login/${formdata.email}/${formdata.password}`)
  //     console.log(response)
  //   }catch (err){
  //     throw err
  //   }
  //   alert ("login successfull")
  //   setModal(!modal)
  // }
  
  // console.log(formdata)
  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      let response = await axios.get(`http://localhost:3005/login/${formdata.email}/${formdata.password}`);
      console.log(response);
      loginUser({
        email: formdata.email,
        // Add other user-related information if available
      });

      // Redirect to the homepage on successful login
      navigate('/collections');  // Replace '/homepage' with your actual homepage route
    } catch (err) {
      throw err;
    }
    // alert('Login successful');
    setModal(!modal);
  };

  console.log(formdata);
  

  return (
    <div>
      <Button color="white" onClick={toggle}>
        Login
      </Button>
      <Modal isOpen={modal} toggle={toggle} {...args}>
        <ModalHeader toggle={toggle}>Signin</ModalHeader>
        <ModalBody>
        <>
  
  <Form onSubmit={handleSubmit}>
    <FormGroup floating>
      <Input
        id="exampleEmail"
        name="email"
        placeholder="Email"
        type="email"
        value = {formdata.email}
        onChange={handleInput}
        required
        
      />
      <Label for="exampleEmail">
        Email
      </Label>
    </FormGroup>
    {' '}
    <FormGroup floating>
      <Input
        id="examplePassword"
        name="password"
        placeholder="Password"
        type="password"
        value={formdata.password}
        onChange={handleInput}
        required
      />
      <Label for="examplePassword">
        Password
      </Label>
    </FormGroup>
    {' '}
    <Button type='submit'>
      Login
    </Button>
  </Form>
</>
        </ModalBody>
        {/* <ModalFooter>
          <Button color="white" onClick={toggle}>
            <Registration/>
          </Button>{' '}
          <Button color="secondary" onClick={toggle}>
            Cancel
          </Button>
        </ModalFooter> */}
      </Modal>
    </div>
  );
}

export default Login;



