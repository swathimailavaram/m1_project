import React from 'react'
import {
    Navbar,
    NavbarBrand,
    Nav,
    NavLink,
    CardGroup,
    Button
    
  
  } from 'reactstrap';
  import Carousel from 'react-bootstrap/Carousel';
  import Card from 'react-bootstrap/Card';
import { useUser } from './UserContext';
  

function Home() {
  
    const imageStyle ={
        width:'100%',
        height:'500px',
        objectFit: 'cover',
        padding:'20px'
      };
      const cardStyle = {
        border: '0', // Set border to 0 to remove the border
      };
      
  return (
    <div>
        <div style={{paddingTop:"50px"}}>
          <h1>hello</h1>
        
         <marquee><h6 style={{background:'goldenrod',textAlign:'center'}}>Today: Gold 24k-10g = RS. 62,800 | Today: Silver-10g = RS.800
          | Today: Platinum-10g = RS. 33,800</h6></marquee>  
      <Carousel >
      
<Carousel.Item>
  <img style={imageStyle}
  src=" https://www.zayangold.com/slider/1196982202.jpg"
  alt="Second Slide"></img>
 
</Carousel.Item>
<Carousel.Item>
  <img style={imageStyle}
  src="https://www.zayangold.com/slider/1441416778.jpg"
  alt="Second Slide"></img>
  
</Carousel.Item>


<Carousel.Item>
<img 
style={imageStyle}
  src="https://content.jdmagicbox.com/comp/chennai/j3/044pxx44.xx44.130711120404.c1j3/catalogue/jcs-jewel-creations-t-nagar-chennai-gold-jewellery-showrooms-avj5sr60zc.jpg"
  alt="Third Slide"></img>
 
</Carousel.Item>

</Carousel>
</div>

<div>

<CardGroup>



<Card style={cardStyle}>
  <Card.Img variant="top" src="https://tiimg.tistatic.com/fp/1/008/532/nri-gold-jewellery-213.jpg" style={{padding:10,borderRadius:100}}/>
  
</Card>
<Card style={cardStyle}>
  
  <Card.Body>
    <Card.Text>
       
        <br></br>
    <h1>Gold</h1>
    <br></br>
    
        <h5>Gold, the precious yellow metal has always been the favorite world over as a jewelry material. 
      It is also considered as auspicious in India. We at MVK excel in crafting exquisite jewelry out of gold.
      gold is a very soft metal. It's too delicate for everyday wear, so it's often alloyed 
      (or mixed) with other metals such as silver, copper, nickel, and zinc to improve its strength and resilience.
       The most common mixtures of gold are 14K, 18K, and 22K, but 14K and 18K are the most ideal for jewelry.
       Our designs are contemporary, casual as well as inspired by the ethnic ornaments of the bygone era. Along with 
       the best designs we ensure the purity and quality of the gold in our jewelry. You will find jewelry for all occasions and especially 
      gold designs for the special occasion such as wedding & festivals. We also have gold ornaments designed for men, 
      kids and ever demanding teens.</h5>
    </Card.Text>
  </Card.Body> 
</Card>
</CardGroup>
<CardGroup>


<Card style={cardStyle}>
  <Card.Img variant="top" src="https://3.imimg.com/data3/LK/UT/MY-6998490/bridal-diamond-necklace-sets.jpg" style={{padding:10,borderRadius:100}}  />
  
</Card>
<Card style={cardStyle}>
  
  <Card.Body>
    
    <Card.Text>
        <br></br>
      <h1>Diamond</h1>
      <h5>Diamonds are said to be a woman’s best friend. Certainly so. A beautiful piece of diamond adds that touch of
         glamour and style with ease. Besides diamonds are a clever investment too, as they have huge resale value.
          We at Zayan showcase a huge collection of diamond jewelry, from rings to heavy necklaces. The best quality 
          diamonds are used in our ornaments with special care taken to apply the perfect cut.
          They are composed of carbon, crystallize under extremes temperatures and pressure, and actually form to 
          become the hardest gemstones available. A diamond can only be scratched by
           another diamond which is one of the characteristics that makes them a durable option for the fine 
           ewelry that you wear every day. When you buy from us you can be 
        assured of the quality diamond that you get. Check out our huge catalogue of diamond jewelries to find your special one.</h5>
    </Card.Text>
  </Card.Body>
  
</Card>
</CardGroup>
<CardGroup>



<Card style={cardStyle}>
  <Card.Img variant="top" src="https://i.etsystatic.com/24534712/r/il/5c6aeb/3157469110/il_570xN.3157469110_8u70.jpg" style={{padding:10,borderRadius:100,height:450}}/>
  
</Card>
<Card style={cardStyle}>

  <Card.Body>
    <Card.Text>
        
        <br></br>
    <h1>Sliver</h1>
        <h5>Silver has always been a traditional jewelry material in India. It is
             cheaper but no less beautiful than gold. If you wish to experiment with 
             different designs at affordable prices, then silver is the definite choice for you. 
             Similar in its composition and character to gold and copper, silver is a very soft,
              ductile and malleable metal which also takes a very high polish. While it doesn't have the hardness of gold, 
             it still has many uses, especially when alloyed with other metals to make it harder.

             At MVK you will find huge collection of silver jewelry that is sure to fascinate you. 
             The designs are inspired by rural and rustic ornaments worn by women in India as well 
             as smart international contemporary designs. There is a silver design available at Zayan
             for every mood and everyone. We ensure the highest possible checks to deliver silver 
             ornaments of impeccable quality and purity..</h5>
    </Card.Text>
  </Card.Body> 
</Card>
</CardGroup>
<CardGroup>
<Card style={cardStyle}>
  <Card.Img variant="top" src="https://www.businessinsider.in/photo/45148034.cms" style={{padding:10,borderRadius:100,height:400}}/>
  
</Card>
<Card style={cardStyle}>
  {/* <Card.Img variant="top" src="https://promos.makemytrip.com/images/AirArabia-tb-dt-011123.webp" style={{height:"400px",padding:"20px"}} /> */}
  <Card.Body>
    <Card.Text>
        
        <br></br>
    <h1>Platinum</h1>
        <h5>Platinum jewelry does not fade or tarnish and keeps its looks for a lifetime.
             Platinum's purity makes it hypoallergenic and ideal for those with sensitive skin. 
            All the platinum ever mined would fit in the average living room. Platinum is rare, the coveted treasure of discerning individuals.
            Platinum ornaments are very attractive and durable.
             At MVK gold you can experience a wide range of Platinum ornaments with high levels of Quality Mark..</h5>
    </Card.Text>
  </Card.Body> 
</Card>
</CardGroup>

    </div>
    
    </div>
  )
}

export default Home