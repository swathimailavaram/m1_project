import React from 'react';
import {NavLink} from 'reactstrap';
import {NavDropdown ,Navbar, Container, Nav, Row, Col, Form, Button, } from 'react-bootstrap'
import {Link} from 'react-router-dom'
import Registration from '../Registration';
import Login from './Login';
import { BiCart } from 'react-icons/bi';
import { useCart } from 'react-use-cart';
import { useUser } from './UserContext';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUser } from '@fortawesome/free-solid-svg-icons';


function Navbarr() {
  // const { user } = useUser();
  const { user, logoutUser } = useUser();

  const linkStyle = {
    textDecoration: 'none', // Removes underline
    color: 'black', // Set your desired color
  };
  const {
    isEmpty,
    totalItems,
  } = useCart();
  return (
    <div>
      
        <nav class="navbar navbar-expand-lg bg-body-tertiary fixed-top" style={{borderBottom:"2px  solid gray"}}>
       
   <div class="container-fluid"  >
   <img
            alt="logo"
            src="https://mir-s3-cdn-cf.behance.net/projects/404/308f90140253757.Y3JvcCwxNTAwLDExNzMsMCwzODk.png"
            style={{
              height: 90,
              width: 180,
            }}
          />
  
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button><span></span>
    <h2 style={{color:'goldenrod',textAlign:'right'}}>   MVK Jewellers</h2>
    <div class="collapse navbar-collapse" id="navbarNavDropdown" >
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="#" style={{marginLeft:400}} ><Link to="/" style={linkStyle}>home</Link></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#"><Link to="/Collections" style={linkStyle} >Collections</Link></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/Aboutus"><Link to="/Aboutus" style={linkStyle} >About us</Link></a>
        </li>
        <li class="nav-item ">
          <a class="nav-link " href="#">
          <Link to="/Contactus" style={linkStyle} >Contact</Link>
            
          </a>
          
        </li>
        {/* <li class="nav-item">
        <Registration/>
        </li> */}
      
        {/* <li class="nav-item">
        <Login/>
        
        </li> */}
       
      
        {/* <li class="nav-item" style={{background:"white"}}>
        <Registration/>
        
        </li> */}
        {/* <Nav.Link href="/cart" className='d-flex'>
                  <BiCart size="2rem" />
                  {!isEmpty && <span className='mt--5 text-danger  '>{totalItems}</span>}
                </Nav.Link> */}

      <li class="nav-item">
      <a class="nav-link " href="#">
          <Link to="/Cart" style={linkStyle} ><BiCart size="2rem" />
          {!isEmpty && <span className='mt--5 text-danger  '>{totalItems}</span>}
                  </Link>
            
          </a>

      </li>
      <li class="nav-item" style={{background:"white"}}>
        <Registration/>
        
        </li>
        <li className="nav-item" style={{ background: "white" }}>
              {/* Render Login or Logout based on user status */}
              {!user ? (
                <Login />
              ) : (
                <div className="d-flex align-items-center">
                  <li className="nav-item" style={{ marginLeft: "10px", background: "white" }}>
                    <p>Hi, {user.email}</p>
                  </li>
                  <li className="nav-item">
                    <FontAwesomeIcon icon={faUser} size="1x" />
                  </li>
                  <li className="nav-item">
                    <button className="nav-link" style={{ background: "white", border: "none", cursor: "pointer" }} onClick={logoutUser}>
                      Logout
                    </button>
                  </li>
                </div>
              )}
            </li>
      
            
      </ul>
      
    </div>
    
  </div>
  
</nav>



    </div>
  )
}

export default Navbarr