import React from 'react'
import Card from 'react-bootstrap/Card';
import { CardGroup } from 'reactstrap';

function Aboutus() {
  // const cardStyle = {
  //   border: '0', // Set border to 0 to remove the border
  // };
  // const imageStyle = {
  //   height:"100px",
  //   width:"100px",
    
    
  // }
  
  return (
//    
<div style={{paddingTop: 120}}>
<marquee><h6 style={{background:'goldenrod',textAlign:'center'}}>Today: Gold 24k-10g = RS. 62,800 | Today: Silver-10g = RS.800 | Today: Platinum-10g = RS. 33,800</h6></marquee>
        
 <center> <h2>Reasons for why MVK Jewellers</h2></center>
<div style={{ display: 'flex', alignItems: 'center' }}>
  <img
    src="https://www.lalithaajewellery.com/assets/img/why/1.jpg"
    alt="Description"
    style={{ marginRight: 10, width: '100px', height: '100px' }}
  />
  <h3>Manufacturing Price</h3><br></br>
  
</div>
<p>We are the only people to give Gold Jewellery at Manufacturing price, for the lowest wastage.</p>
<div style={{ display: 'flex', alignItems: 'center' }}>
  <img
    src="https://www.lalithaajewellery.com/assets/img/why/2.jpg"
    alt="Description"
    style={{ marginRight: 10, width: '100px', height: '100px' }}
  />
  <h3>Lowest Price</h3><br></br>
  
</div>
<p>Despite maintaining high-quality standards, we offer the lowest prices for Gold, Diamond, and Silver Jewellery obviating the need for bargaining.</p>
<div style={{ display: 'flex', alignItems: 'center' }}>
  <img
    src="https://www.lalithaajewellery.com/assets/img/why/4.jpg"
    alt="Description"
    style={{ marginRight: 10, width: '100px', height: '100px' }}
  />
  <h3>Superior Gift</h3><br></br>
  
</div>
<p>Though we give no gifts along with your purchase, what we offer in terms of value is far superior to any gift you may get.</p>
<div style={{ display: 'flex', alignItems: 'center' }}>
  <img
    src="https://www.lalithaajewellery.com/assets/img/why/9.jpg"
    alt="Description"
    style={{ marginRight: 10, width: '100px', height: '100px' }}
  />
  <h3>Zero Charges</h3><br></br>
  
</div>
<p>No extra charges apply for credit card and debit card payments. So, the net effect is the
   same as a cash payment. (For gold coins, 2% is charged extra).</p>
<div style={{ display: 'flex', alignItems: 'center' }}>
  <img
    src="https://www.lalithaajewellery.com/assets/img/why/10.jpg"
    alt="Description"
    style={{ marginRight: 10, width: '100px', height: '100px' }}
  />
  <h3>Old is Gold</h3><br></br>
  
</div>
<p>For the exchange of Old gold Jewellery purchased from us for new jewels, we offer 100% exchange value for the gold you bring 
  (as per the day's gold rate) when produced with our purchase receipt..</p>
<div style={{ display: 'flex', alignItems: 'center' }}>
  <img
    src="https://www.lalithaajewellery.com/assets/img/why/11.jpg"
    alt="Description"
    style={{ marginRight: 10, width: '100px', height: '100px' }}
  />
  <h3>Easy Exchange</h3><br></br>
  
</div>
<p>You can exchange a jewel purchased within 7 days from the date of purchase. (Needless to say, such jewels must not have been used)..</p>
</div>

  )
}

export default Aboutus