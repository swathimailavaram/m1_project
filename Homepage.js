import React from 'react'
import Registration from './Registration'
import Login from './components/Login'
import {
    Navbar,
    NavbarBrand,
    Nav,
    NavLink,
    CardGroup,
    Button
    
  
  } from 'reactstrap';
  import {
    MDBFooter,
    MDBContainer,
    MDBCol,
    MDBRow,
    MDBIcon,
    MDBBtn
  } from 'mdb-react-ui-kit';
  
  import Carousel from 'react-bootstrap/Carousel';
import Card from 'react-bootstrap/Card';


function Homepage() {
    const imageStyle ={
        width:'100%',
        height:'350px',
        objectFit: 'cover',
        padding:'20px'
      }
  return (
    <div>
        <Navbar className="my-2" color="secondary" dark>
        <NavbarBrand href="/">
          <img
            alt="logo"
            src="https://imgak.mmtcdn.com/pwa_v3/pwa_hotel_assets/header/mmtLogoWhite.png"
            style={{
              height: 90,
              width: 180,
            }}
          />
          <div style={{display:'flex',marginLeft:800,}}>
          <Login/> <Registration/>
          </div>
        </NavbarBrand>
       
      </Navbar>
      <div style={{padding:"20px"}}>
      <Carousel >

      
<Carousel.Item>
  <img style={imageStyle}
  src="https://aviacargo.si/wp-content/uploads/2019/09/aviacargo-slider-img-1.jpg"
  alt="Second Slide"></img>
  <Carousel.Caption>
    <h3>Second slide label</h3>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
  </Carousel.Caption>
</Carousel.Item>


<Carousel.Item>
<img 
style={imageStyle}
  src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQAqAOVSEOSRYyfYB55LOqTKwJ-wC8KfFCIjoZ88LSMuCpWOVOZxiMOmV_n1dK0oAGHTA&usqp=CAU"
  alt="Third Slide"></img>
  <Carousel.Caption>
    <h3>Third slide label</h3>
    <p>
      Praesent commodo cursus magna, vel scelerisque nisl consectetur.
    </p>
  </Carousel.Caption>
</Carousel.Item>

</Carousel>
</div>

<div>
    <h3>Offers</h3>
<CardGroup>
<Card>
  <Card.Img variant="top" src="https://promos.makemytrip.com/images/intl-sale-tb-dt-141123.webp" style={{height:"400px",padding:"20px"}}/>
  <Card.Body>
    <Card.Title>INTL FLIGHTS</Card.Title>
    <Card.Text>
      grab upto 15% + no emi
    </Card.Text>
  </Card.Body>
  <Card.Footer>
  <Button
    color="primary"
  >
    Book Now
  </Button>
  </Card.Footer>
</Card>
<Card>
  <Card.Img variant="top" src="https://promos.makemytrip.com/images/dh-offer-310723.webp" style={{height:"400px",padding:"20px"}} />
  <Card.Body>
    <Card.Title>INTL HOTELS</Card.Title>
    <Card.Text>
      grab upto 25%.{' '}
    </Card.Text>
  </Card.Body>
  <Card.Footer>
  <Button
    color="primary"
  >
    Book Now
  </Button>
  </Card.Footer>
</Card>
{/* <Card>
  <Card.Img variant="top" src="https://images.pexels.com/photos/1640773/pexels-photo-1640773.jpeg?auto=compress&cs=tinysrgb&w=600" />
  <Card.Body>
    <Card.Title>Card title</Card.Title>
    <Card.Text>
      This is a wider card with supporting text below as a natural lead-in
      to additional content. This card has even longer content than the
      first to show that equal height action.
    </Card.Text>
  </Card.Body>
  <Card.Footer>
    <small className="text-muted">Last updated 3 mins ago</small>
  </Card.Footer>
</Card> */}
<Card>
  <Card.Img variant="top" src="https://promos.makemytrip.com/images/holiday-thumb-190923.webp"  style={{height:"400px",padding:"20px"}} />
  <Card.Body>
    <Card.Title>INTL HOLIDAY</Card.Title>
    <Card.Text>
      Grab upto 30%
    </Card.Text>
  </Card.Body>
  <Card.Footer>
  <Button
    color="primary"
  >
    Book Now
  </Button>
  </Card.Footer>
</Card>
<Card >
  <Card.Img variant="top" src="https://promos.makemytrip.com/images/AirArabia-tb-dt-011123.webp" style={{height:"400px",padding:"20px"}} />
  <Card.Body>
    <Card.Title>INTL FLIGHTS</Card.Title>
    <Card.Text>
      LANUCHED:New flight is lanuched by Arabia.{' '}
    </Card.Text>
  </Card.Body>
  <Card.Footer>
  <div>
  <Button
    color="primary"
  >
    Book Now
  </Button>
</div>
  </Card.Footer>
</Card>
</CardGroup>



<div>
<MDBFooter className='bg-dark text-center text-white'>
      <MDBContainer className='p-4 pb-0'>
        <section className='mb-4'>
          <MDBBtn outline color="light" floating className='m-1' href='#!' role='button'>
            <MDBIcon fab icon='facebook-f' />
          </MDBBtn>

          <MDBBtn outline color="light" floating className='m-1' href='#!' role='button'>
            <MDBIcon fab icon='twitter' />
          </MDBBtn>

          <MDBBtn outline color="light" floating className='m-1' href='#!' role='button'>
            <MDBIcon fab icon='google' />
          </MDBBtn>
          <MDBBtn outline color="light" floating className='m-1' href='#!' role='button'>
            <MDBIcon fab icon='instagram' />
          </MDBBtn>

          <MDBBtn outline color="light" floating className='m-1' href='#!' role='button'>
            <MDBIcon fab icon='linkedin-in' />
          </MDBBtn>

          <MDBBtn outline color="light" floating className='m-1' href='#!' role='button'>
            <MDBIcon fab icon='github' />
          </MDBBtn>
        </section>
      </MDBContainer>

      <div className='text-center p-3' style={{ backgroundColor: 'rgba(0, 0, 0, 0.2)' }}>
        © 2020 Copyright:
        <a className='text-white' href='https://mdbootstrap.com/'>
          MDBootstrap.com
        </a>
      </div>
    </MDBFooter>
</div>

{/* <CardGroup>
<Card>
  <Card.Img variant="top" src="https://promos.makemytrip.com/images/holiday-thumb-190923.webp"  style={{height:"400px",padding:"20px"}} />
  <Card.Body>
    <Card.Title>INTL HOLIDAY</Card.Title>
    <Card.Text>
      Grab upto 30%
    </Card.Text>
  </Card.Body>
  <Card.Footer>
  <Button
    color="primary"
  >
    Book Now
  </Button>
  </Card.Footer>
</Card>
<Card>
  <Card.Img variant="top" src="https://promos.makemytrip.com/images/AirArabia-tb-dt-011123.webp" style={{height:"400px",padding:"20px"}} />
  <Card.Body>
    <Card.Title>INTL FLIGHTS</Card.Title>
    <Card.Text>
      LANUCHED:New flight is lanuched by Arabia.{' '}
    </Card.Text>
  </Card.Body>
  <Card.Footer>
  <div>
  <Button
    color="primary"
  >
    Book Now
  </Button>
</div>
  </Card.Footer>
</Card> */}
{/* <Card>
  <Card.Img variant="top" src="https://images.pexels.com/photos/70497/pexels-photo-70497.jpeg?auto=compress&cs=tinysrgb&w=600" />
  <Card.Body>
    <Card.Title>Card title</Card.Title>
    <Card.Text>
      This is a wider card with supporting text below as a natural lead-in
      to additional content. This card has even longer content than the
      first to show that equal height action.
    </Card.Text>
  </Card.Body>
  <Card.Footer>
    <small className="text-muted">Last updated 3 mins ago</small>
  </Card.Footer>
</Card> */}
{/* </CardGroup> */}
</div>








        
    </div>
  )
}

export default Homepage