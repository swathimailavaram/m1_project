import React from 'react';
import {
  Navbar,
  NavbarBrand,
  Nav,
  NavLink,
  CardGroup
  

} from 'reactstrap';

import Carousel from 'react-bootstrap/Carousel';
import Card from 'react-bootstrap/Card';




import Login from './components/Login';
import Registration from './Registration';


function Mainpage() {
 
  const imageStyle ={
    width:'100%',
    height:'350px',
    objectFit: 'cover',
  }


  


  return (
    <div>
      <Navbar className="my-2" color="dark" dark>
        <NavbarBrand href="/">
          <img
            alt="logo"
            src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCA8RDxEPEREPERAQEQ8REBIREhESGBEQGBgZGRgUGBgbIS0mGx0rIRUYJTclKi4xNDQ0GyM6Pzo3Pi0zNDEBCwsLDw8QGBERGDMhISI+MzExMT40MTExMTMxMzMzMTEzNDE0MTMzMzEzMzE+MzMzMzEzMzEzMzEzMTEzMTMzMf/AABEIALcBEwMBIgACEQEDEQH/xAAcAAACAgMBAQAAAAAAAAAAAAAAAgEFAwYHBAj/xABLEAABAwIDAwUKCQsCBwAAAAABAAIDBBEFEiEGEzFBUWFxgQcUIjJSkZKTocFCQ1NygrGy0dIVFhcjMzRUYnODomOzJCU1RHTD8P/EABsBAQEAAwEBAQAAAAAAAAAAAAABAgQFAwYH/8QAMhEBAAEDAgMFBgUFAAAAAAAAAAECAxEEIQUSMRNBUZGhIlJhscHRFnGB4fAGFSMzQv/aAAwDAQACEQMRAD8A6OhCFUCEJwEEAJwEAJgEUAKQEAJgEQAJgFICLKKAFICkBACAspsiymyCLIsmsiyBbKbKbIsgiyLKbIQJZFk6LIMdlFk5CiyBSEhCyEJSEGMhQQshCUhVCEJSExCghFYyFCchKURCEIQCEIQCEIQSFkASgJgEVICYBQEwCIkBMAgKbKCQFICAFICKAFNlKSonZGwvkcxjBxc9waPOUOs4PZTZaziG2tHFdseed48mzG3+c73ArVsR21rZdGFsDf8ATBv2uOvmsjqafg+rvb8vLHjO3p19HSaushhGaaSOMc73Bt+ocvYtdq9tacOEVOx88hcGNPiNLibAc51PMubSzPe4ue8uceLi4lx6ydVs/c/wze1O/cPApwHN6ZDo3zDMewK4dSrg+n01qq9fqmvljp0jPdHjOZxvl0mIOytz5c9hmy3tmtrboT2TWRZR8uSyLLzVGIRR6eHI7hlhY6V1+YhoOXtsqqpr8Tlu2npBE3kfUuZm9G+ntRsWtNXc32pjxmYpj16/pldyPa1pc4hrRqXOIAA6SVQ4jtbSREtjLp5OAEQGW/zj7rqsn2Vr5/Cq6tlhroXyBvULBrexeUYbgtI8PqMRiL2Fr8u8hFy05gC3V3JwRv29Pw+1vevc8+FMTEecx9m9sJLQXCziBcXvY21F0ELTq3unYRHcNkmncL6RQv16nSZR7VOy+3ceJVZpoaWVjGRPlfJI9lwGlrQMjb8S/wArkRyG3kJSE5CghAhCQhZCEpCIxkJSFkISlUIQlITlQQisSFJUIgQhCATBKnCKkJwlCYIJCcBQEwRDBSFAUlpIIBsSCAeY86iiR7WNLnOa1o4ucQAOslUGJbZUMFw1++cOSIAj0jp5rrWsUpzK9wndI9zXOBD5HkNINjZpNh2KvOD0/kn03/eo+h03CbHW7XNXwjaPPrPo9eI7e1Ml2xBkA52jM70naexazVYjJK/NJI57vKc9ziOgX4BXBwen8g+m/wC9KcHp/IPpv+9My7tiNPY/1Ucvz8+vqoTOEpqQr44NT+QfSf8AelOD0/kH03/epmpsdv8ABRd8gkDnXatk8M72oomEWkeBLJz53geCeoBo7FzKPDIWOa5rNWua5ty5wzA3FwTYjTgVsbtqK/5Qeqj+5Iz3uZxSxf1VFNFGIiN5z6d0t+nkLBcRvkPMzJf/ACcB7VS1uL4i2+4wqaQjhvaqkhB6fBc8rWfzqr/lB6qP7kp2qxD5Qeqj/CsnGjgWp8afX7PXWYntU/SLDqKEfzTMlI7d40exUFZSbZy8XujB+DFJSR+1pze1WJ2sr/lB6uP8Kg7WYh5Y9VH+FFjgOp96nzn7NQq9g9oZv2zJJuX9dVxya/TeV5x3NsZH/asH96D8S3R21uI/Kj1cX4VB2txH5Qeqi/Cix/T+qnvp85+zTP0b41/DN9dB+Jb93L9laqg77kq4xG+XcsjAex/gNzOcbtJtcub6K8Dtr8RHGUD+1F+FL+eGI/Kt9VF+FF/D2q96nzn7OnqCFzH88MQ+Vb6qL8Kn878Q+Vb6qP8ACh+HtT71PnP2dMISla9sfiFTVMllneHAFsbAGtFnWuToBfi1bEUcjUWKrFyq1VMTMeH74IUhWUrGUeBClKcpSqpCEiyFIUEIQhESE4ShMEUwTBQEwQMEygJgoJCYJQuX7Qd0PE6GqkpZaWkzMN2OtMBJGdWPb4XAjzEEciDZtpqbJPnHiytv9Pgfce1UhV5S4iMTwiOsDWtkGZ0jG3sx7Lte0X1t8IdFlRlH1HDbvaWIz1p2+3oUrzVk27je/laNOs6D2lekqr2hmDKSZ54Dd+17R70bl+qabVdUdYiZ8oVcFI+cPeXXLfK1zG17dC9eC1BdmjcSQAHNvyDgR9SxbL1bZIJHC9myW1+aD714NlcQZLM5ovcRF2vzmj3o4ultxauaaunrXzc3xemrZmqXMuRmeB1XASzB1PIPCJ0DuWxF+BHYvJWYpHHXOa8kBkjcxsSALNN9OtLiGLxzTtjhzSXAYCGu1cb844C4160a9cURF2uJ/wAkV+zvOfJdYv8Asx8/3LwCmO63oceXThbXLxWfaSpbFC1xvbeZdPmn7ligq2nDjNrlDXnzSEe5HQ1dim7rLsVx0omY/OMYe3D5S9oJ1LTa/OFVwxZzlBtxOqyYBXtka8tB0fGNdOdVtBi0W8OZ2QWdqdBfmRjcxet6Htp2qmqJnPdExHXK7p2FlxfNcjn/APuVeKnjzG1wNL8F6aaojeM0bmvAcAS3kPG3tCqMOxmHMczi0ZeJB43GmiNnXWNNTc0lvMdn7Xftjbvz4rGAkPy35wfMvYqegr2zznIHEAucSQQALaX69Fbqw3OBzPY1xnNMVTFP5bJ8yAi3QslNC6SRkbbZnuDW/OJAH1ph2cxG89HT9kqXdUEIPGS8p6Q/xf8AHKrcqIYgxjY26NY1rG9QFh9SkqPza/dm7cquT/1Mz5lKUpykKPEhSlOUhVClIU5SlFIhCEQ4TBKEwRTBMEoThEME4SpgoqQtQ7pGy/f9LvIm3qqYOfFbjIzi+LrNrjpFuUrcAmCDkfcYxUZ6rDpNWSt74jBv4wsyRva0sNv5XK6roDHJJGfguLesch7RYqp2zws4TitPjULSKaScGpa0G0b3XEmg5Htc4j+YHnC2/a2n8KOdti2RuUkcCRq09oP+KOpwq9y3Zo975x/Ja6VSbW/uFR/a/wB1iuyqvaKnMtHOxoJcWZmgcSWkOsPRR39REzariO+J+Sl2F/dZv6x+w1VGwn71J/47vtxrJsxjcFPTzMkLsznbxgDS7OSMuW44atHHnTbCQkPmlOjWMay50GZxDiP8R5wjj6e5TXXo6aZzMc2fg81fCyTF3MeLsfMxrhci4yN5lGPQNoquN0GZgyNktmcbG7gRc8hDRp1qaqT/AJwXDX9fHb0GrHti8mdpJ13I+05HnctxGn1N3HtU17T3xuuNs5WmnYB8sDf6Ll5aZx/JRF9AyTT+45TtT+7s/qt+y5eGPEIm4fubky2e3LY6XkLr34cFHT1Ndu1rb01ziJtz5zj+Ye3Y/wAWT58fvVPhFGyacxvzZcsjvBNjcW+9XeycREJefjJWlvSG6X891TYNVxw1BfISG5ZG6AnU2+5VrVRR2PD4u45faznpiZid/wBMNow6ijgaWR5spOY5jfWwHuWr7OUsUspZI3MBGXAXI1u0cnWVtNBXRz5jGXHIQDcEakLXNkf27v6DvtNVbuposV6jRU0RE0e1jwxsbDLw4gYWl2QvkZa/EWcW36rDVbV7OgLVY/8Aqv8Add9krbD5hzlIe3BtqL1MdKa6oj4Ft0edXux1LvK+I2GWPNK7l8Xxf8i1UdusreO53S/t5yLeLEz7bvqarPRt8Tu9npblXwx57fVuhSlMVBWL4ApSFOUpQKVjKcpCqhSlKcpCisaFKEQwThKEwRUhOEoTBEOnCQJwoqQmCUJgg82J4dFVU8lLM3NFMwseOBHM4HkcDYg84CocNopjhjqGbwp6G8QdY/rWRi8TxfymFo68w5FtIUhovmsLkAE84FyB7T5yjKiuaKoqjrG7mJCxvcALkgDnOi2Gt2YqTK/c7ndlzi0ve5tmnUCzWnhwXidsLWP1fNB2PlNuoZVH19Gt0sxzVXYp9Z9Gj1+F0D3F+5BcSSS18jGk8pytI9yaJjWMDGNDWC9mt0APP19K3lvc/cBd9TG0DUkRuIA6y4KhxaPBaS7ZMS3z28Y6aISOvzEhxa0/OIVZW9fwuzM1W5jM9Zimc/JrhoIDLvcg3lw7NmdxHLa9lNVQwTHNIwOIFgbuGnYeleHEtpoL2pYZcvlVL4ySPmMFh6RVadoqj/SH0UYVcW4byzHLmJ3mOTrPjOcNjqKaKVobI0OaDcC5FiBbkPSsAwilGu5b2uefYStedtBU+W0dUcfvCwPxyp+Wd2NYPqCPK5xnQVTzVWZqn400/WW7NFrWFgOAHALx/kSmPxPHU+FJ9609+Kzn46XseR9SxGulPxkh63OPvR53eOaa5jmsc2OmeX92+UtFHEHCOMMzWzavN7dZWKnoaaBxcxrGG2W+d2o0NtT0BatRYJWVADmsa1juD55ooWkc95HC/Yr2i2KpeNVjGFwAcWwyipeOg2ygHtKuXh/fLccvLpqfZ6bxt+WKdv0Z3toWyb5zoBLmzBxlPjc9r2WZ2KUoIG/jJPCxJ81lsGzuyWy8k7aZtVNXTyBzmsc6WJpDRd1ixreS5tmXSsOwCgpf3ekpoj5TI2Bx632ue0pljHHq6c9nZppzv3/TDlOH0FRU/sYKl45HbmWNh6nyBrfaunbL4c6mpGRvAbIS+R4BDrOOgFxofBDQrolKUmctPWcVv6qiLdcREZztn6zPiUpSmKUqOaUpSmKUoFKQpylKIQpCnKQqqRCEIhgnCQJgimCcJAnCIdOEiYKKYJglCYIGCYJQmCCtx/Go6GnNRIyR7MzWBsYaTmN7XzEADTjdc4xfunV77tpYKenbyPlc6Z9ufKAGtPpLo+0eHd9UVRTaZnxnJfklb4TD6TWrgRHQR0HkQYsWrsQq799VksoPwHEhnYxtmDsCrPyf/OewAK1cFhcEFd3i3ynntH3KDSM6fOV2J+z0NRsyyRkMYqGQb8SNY3O8scXOaXAXddocLdSr8EoIHbL1kzoYXTNbV5ZCxhe21rWda4sg5W6mYPg+clJumcjW+YLrfc9wijhwyoxipibOWb97A5jX5IoQQ7I12mcua4X6um/q2ipKLFcCficVM2nmgbLICGsDgInkPY4ttmaWtcRfhcFBxoNA5Bw5gmH1BdOr6GCTZGOobDCJ4zEHSMYwPOWo3bruAudOK9eKYDC3ZKOVsMQqBBSzukEbN4Q97HOBda/ivI6kHJ2gXHDnUtHAc5uV0zH6Gni2UoniGETztpbyiNgecwdIfDtfg23HguZt5PmlBYYBiRpKynqx8VMx7/6V7PHoFwX04CDqDcHUEco518qM+B1OXfNjseD8DgqnNfIaePczBuUkmI5M2pHwQHdqMqaZqmKaestvKUqpwTH4awyNjbI3dZc2fJre9rZSfJKtSjK7artVzRcjEx3eqClKkpSjzQUpTFKUClIUxSFVEFK5SUpRSIQhEMEwWMJwinCYJQmCIcJwkCYKKYJgkCYIHCYJAmCBgVxDbXDu9sRqGAWY92+Z8x/hHsDs4+iu3haB3VMOzRwVQGrHOhef5H+EwnqLXD6aDl7gsTgs7gkcEHYdk8VEWGULHNGVzWx3J5XF9r9ZAHaqYxd64JidGGgRskqwzoie1j2Dsa8DsXgrnuZs/TvYbOY6mew8z2y3afOFcbQVjJcGqamO2WSlMg7WjQ9XDsQVeCVRbsrUx2FjDiWvW+Q+9Y8Bq7bKVTAAQYcRF+vOvPgrs2y1QeXc4jfo8N5S4BpstVdMWIW87wgXAavebK19Pa+6NSb8LEZJh7StuxmqDsNq8Ma1uaLCm5QDqPAkY32xBab3LMstNX0ruDiwuHRIx7D9hXcVWJNoaqA+K7D2Rkclw4P+qVyCp7otSW4ThVLa2RsZ0PkQBn/sXNmnxe0Fb53V3Wlo4h8XBI63Q5zWj/bK0P36jrQSDYA+SdepdO7jtW14r8NkILJWiZo5wRu5PYY/auYX5exw962DYbEu9MUpJHGzHSCF5545fA16AS130UHVdgIHRy1sb/GjdGx3J4QdICfYt0KrKKj3dbVvAsJ2U0hP836xpHsv9JWRRu8Rvxfvzd96KfPljPqgpSpKUo0gUiYpCggpCnKxlEQUpUlKVVIhCEQBZAsacIpwmCQJggyBMsYThQOEwSAqQUDhMCkCYIHCrtoMPFVRz09hd7DkvySN8Jh9JrV7wUwKD53c3sPNzLxVNWxmnjO5hydZ5F1rFu5tDUSySCqnhbI97yxjGEDMSSLnW1yVW/odpf42p9XEg03Etq4JcHZh4jnbM3dZnEMyeA/ObHPm4aeKsdBtWyPB58NkZK572zMge0MLGsfY2cS4EWcX8AdCFu/6Hab+NqfVxI/Q7TfxtT6uJBpmx210dHFJS1Mb5aaVzneAGOLMwDXtLHEBzCBwvz8br1bSbY00lGaCggdDC6weSxjGhmbMWsY0nieJNuJ57jaf0O038bU+riR+hym/jan1cSDRNiNo4sOlmfKyV7Jo2tAiDCQ5riQSHOaLWc5PS7URsxl+JFkpikc8FgyZ92WZGi2bLfwW8vOttxXuX0FJTyVM+IVLYoWl7zu4r9DQOVxNgBykhchfU6mzfBubXOtuS9hxQbPtjjbK+rFRGyRjGQxxBsgYHeC57ifBcRb9Zz8ioujkPA8xXlFUfJ9qYVB8n2oPTr28o50chA4c3AjqWBszjYZdSQG66k8gHOtrwXYTFauzhTbhh+MqSYtOcNtnPo26UHcdm8S77oKap+FLEwv6JB4Lx6QcrIqg2NwB+HUYpXzCch73tc1hjDA+xLBcm4zZjfTxjor5BBSlSSoKCCkKYlIUEFKVJSlVClKUxSFFQhCEQKQoQgyBOFjBTAopwmBSApgURkCYJAVIKinBTJAVIKBwVN0qm6BrprpLqboGui6W6m6CboUXWs7b19a2n71w+GWWrqQWhzBYQRcHSue6zWu5G3I1ufgoOa91fanvuo7yhfempnHeFp0mqBoT0tZqB05jzFc83QGpXUcF7kc77OrKhkLfk4BvH26Xu8Fp6g5b/guxWF0dnR0zHyN1Es/619+cF2jfogIOGYLsfiNZYwUshYfjZBu2W5w91s30blb/AIL3IGizq2pLjoTFTDKOkGR4uR1NHWurXUXQVODbN4fRD/hqaKN1rF9s7z1vdd3tVqSi6hAKCUEqCUEEqCUEpSUAUhKklKVUQUpUlKSioKRSVCIEIQgEIQgYJgVjTgopwVIKUFSCgyApwViBUgojJdTmWNTlUU+8UGUJCxKY0GQzhKaoLGYSkNOUGU1YSmtCwOpCkNEUHq7/ABzo7/HOvEcP61Aw7rQe4Vw51IrAvG2hKZtIUHsFUEwqAvK2mKyCEoPRvgp3iwCJMI0GXMozJMqLIGJSkoSkqoCVBKglQSiglISmJWNAIQhECEIQCEIQCEIQMCmBUIQOCmBQhBIKYFCEDAqboQoqboQhAKUIQCEIQSoUIQCEIQRdQShCCLpSUIQKSoJQhUKSoJUIQKVCEIgQhCAQhCD/2Q=="
            style={{
              height: 90,
              width: 180,
            }}
          />
        </NavbarBrand>
        <Nav className="ml-auto flex-column" navbar>
          <NavLink>
            <Login />
          </NavLink>
          <NavLink>
            <Registration />
          </NavLink>
        </Nav>
      </Navbar>

      

      <Carousel>

      
      <Carousel.Item>
        <img style={imageStyle}
        src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQFpDfVe8B1o8V2GGW6f8UehxX9Z2XGMHn4FA&usqp=CAU"
        alt="Second Slide"></img>
        <Carousel.Caption>
          <h3>Second slide label</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
        </Carousel.Caption>
      </Carousel.Item>


      <Carousel.Item>
      <img 
      style={imageStyle}
        src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRzNkCIN4ECONOmSO9rTF2_oJedSyPpIQ-8vw&usqp=CAU"
        alt="Third Slide"></img>
        <Carousel.Caption>
          <h3>Third slide label</h3>
          <p>
            Praesent commodo cursus magna, vel scelerisque nisl consectetur.
          </p>
        </Carousel.Caption>
      </Carousel.Item>
      
      </Carousel>
    
    
      <CardGroup>
      <Card>
        <Card.Img variant="top" src="https://images.pexels.com/photos/106343/pexels-photo-106343.jpeg?auto=compress&cs=tinysrgb&w=600"/>
        <Card.Body>
          <Card.Title>Card title</Card.Title>
          <Card.Text>
            This is a wider card with supporting text below as a natural lead-in
            to additional content. This content is a little bit longer.
          </Card.Text>
        </Card.Body>
        <Card.Footer>
          <small className="text-muted">Last updated 3 mins ago</small>
        </Card.Footer>
      </Card>
      <Card>
        <Card.Img variant="top" src="https://images.pexels.com/photos/769289/pexels-photo-769289.jpeg?auto=compress&cs=tinysrgb&w=600" />
        <Card.Body>
          <Card.Title>Card title</Card.Title>
          <Card.Text>
            This card has supporting text below as a natural lead-in to
            additional content.{' '}
          </Card.Text>
        </Card.Body>
        <Card.Footer>
          <small className="text-muted">Last updated 3 mins ago</small>
        </Card.Footer>
      </Card>
      <Card>
        <Card.Img variant="top" src="https://images.pexels.com/photos/1640773/pexels-photo-1640773.jpeg?auto=compress&cs=tinysrgb&w=600" />
        <Card.Body>
          <Card.Title>Card title</Card.Title>
          <Card.Text>
            This is a wider card with supporting text below as a natural lead-in
            to additional content. This card has even longer content than the
            first to show that equal height action.
          </Card.Text>
        </Card.Body>
        <Card.Footer>
          <small className="text-muted">Last updated 3 mins ago</small>
        </Card.Footer>
      </Card>
    </CardGroup>




    <CardGroup>
      <Card>
        <Card.Img variant="top" src="https://images.pexels.com/photos/1279330/pexels-photo-1279330.jpeg?auto=compress&cs=tinysrgb&w=600" />
        <Card.Body>
          <Card.Title>Card title</Card.Title>
          <Card.Text>
            This is a wider card with supporting text below as a natural lead-in
            to additional content. This content is a little bit longer.
          </Card.Text>
        </Card.Body>
        <Card.Footer>
          <small className="text-muted">Last updated 3 mins ago</small>
        </Card.Footer>
      </Card>
      <Card>
        <Card.Img variant="top" src="https://images.pexels.com/photos/70497/pexels-photo-70497.jpeg?auto=compress&cs=tinysrgb&w=600" />
        <Card.Body>
          <Card.Title>Card title</Card.Title>
          <Card.Text>
            This card has supporting text below as a natural lead-in to
            additional content.{' '}
          </Card.Text>
        </Card.Body>
        <Card.Footer>
          <small className="text-muted">Last updated 3 mins ago</small>
        </Card.Footer>
      </Card>
      <Card>
        <Card.Img variant="top" src="https://images.pexels.com/photos/70497/pexels-photo-70497.jpeg?auto=compress&cs=tinysrgb&w=600" />
        <Card.Body>
          <Card.Title>Card title</Card.Title>
          <Card.Text>
            This is a wider card with supporting text below as a natural lead-in
            to additional content. This card has even longer content than the
            first to show that equal height action.
          </Card.Text>
        </Card.Body>
        <Card.Footer>
          <small className="text-muted">Last updated 3 mins ago</small>
        </Card.Footer>
      </Card>
    </CardGroup>
    



      



    </div>
  );
}

export default Mainpage;