const {createStore} = require('redux');
const intialstate = {
    bal: 1000,
    name: "mailavaram",
    id:"",
    accountNo:"",
};
const reducer = (state = intialstate,action) =>{
    const newstate = {...state};

    switch (action.type){
        case "deposit":
            newstate.bal += action.value;
            
            break;
        case "withdraw":
            newstate.bal -= action.value;
            break;
        case "name":
            newstate.name = action.value;
    }
    return newstate;
};
const store = createStore(reducer)

store.subscribe(()=>{
    console.log(store.getState())
})
store.dispatch({type:'deposit',value:10000})

store.dispatch({type:'deposit',value:3000,type:'name',value:"swathi"})
store.dispatch({type:'deposit',value:3000,type:'name',value:"sneha"})


