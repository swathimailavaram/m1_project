import logo from './logo.svg';
import './App.css';
import Homepage from './Homepage';
import Mainpagee from './Mainpage';
import { Navbar } from 'reactstrap';
import Navbarr from './components/Navbarr';
import {BrowserRouter,Routes,Route} from 'react-router-dom'

import Contact from './components/Contact';
import Collections from './components/Collections';
import Nopage from './components/Nopage';
import Aboutus from './components/Aboutus';
import Home from './components/Home';
import Footer from './components/Footer';
import Cart from './components/Cart';
import { UserProvider } from './components/UserContext';

function App() {
 
  return (
    <div>
     <UserProvider>
      <BrowserRouter>
      
      <Navbarr/>
      
      <Routes>
        <Route path="/" element={<Home/>}/>
        <Route path="/Aboutus" element={<Aboutus/>}/>
        <Route path="/Cart" element={<Cart/>}/>
       
        <Route path="/Contactus" element={<Contact/>}/>
        <Route path="/Collections" element={<Collections/>}/>
        

        <Route path="*" element={<Nopage/>}/>


      </Routes>
      </BrowserRouter>
      </UserProvider>
      <br></br>
      <Footer/>
    </div>
  );
}

export default App;
