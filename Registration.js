// import React from 'react'

// function Registration() {
//   return (
//     <div>Registration</div>
//   )
// }

// export default Registration
import React, { useState } from 'react';
import axios from 'axios'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter,Form,Label,Input,FormGroup,FormText} from 'reactstrap';
import { useNavigate } from 'react-router-dom';

function Registration(args) {
  // const navigate = useNavigate();
  const [modal, setModal] = useState(false);

  const toggle = (e) => {
    e.preventDefault()
    setModal(!modal);
  }
  const [formdata,setFormdata] = useState({
    username:'',
    email:'',
    password:'',
   
    select:'',
    address:'',
    radio1:'',




  })
  const handleInput = (e) =>{
    const {name,value} = e.target
    setFormdata({
      ...formdata,
      [name]:value
    })
  }
  const handleSubmit = async (e) =>{
    e.preventDefault();
   try{ 
    
      let  response = await axios.post("http://localhost:3005/register",formdata)
      console.log(response)
    }catch (err){
      throw err
    }
    alert ("registration sucessfully")
  }
  console.log(formdata)
  // const handleSubmit = async (e) => {
  //   e.preventDefault();
  //   try {
  //     let  response = await axios.post("http://localhost:3005/register",formdata)
  //     console.log(response);

  //     // Redirect to the homepage on successful login
  //     navigate('/Login');  // Replace '/homepage' with your actual homepage route
  //   } catch (err) {
  //     throw err;
  //   }
  //   // alert('Login successful');
  //   setModal(!modal);
  // };

  // console.log(formdata);
  

  return (
    <div>
      <Button color="white" onClick={toggle}>
        signup
      </Button>
      <Modal isOpen={modal} toggle={toggle} {...args}>
        <ModalHeader toggle={toggle}>Register </ModalHeader>
        <ModalBody>
        <Form onSubmit={handleSubmit}>
        <FormGroup>
    <Label for="examplePassword">
      username
    </Label>
    <Input
      id="examplePassword"
      name="username"
      placeholder="username"
      type="text"
      value = {formdata.username}
      onChange={handleInput}
    />
  </FormGroup>
  <FormGroup>
    <Label for="exampleEmail">
      Email
    </Label>
    <Input
      id="exampleEmail"
      name="email"
      placeholder="with a placeholder"
      type="email"
      value = {formdata.email}
      onChange={handleInput}
    />
  </FormGroup>
  <FormGroup>
    <Label for="examplePassword">
      Password
    </Label>
    <Input
      id="examplePassword"
      name="password"
      placeholder="password placeholder"
      type="password"
      value = {formdata.password}
      onChange={handleInput}
    />
  </FormGroup>
  <FormGroup>
    <Label for="exampleSelect">
      city
    </Label>
    <Input
      id="exampleSelect"
      name="select"
      type="select"
      value = {formdata.select}
      onChange={handleInput}
    > 
      <option>
        select
      </option>
      <option>
        medak
      </option>
      <option>
        narsapur 
      </option>
      <option>
        hyderbad
      </option>
      
    </Input>
  </FormGroup>
  
  <FormGroup>
    <Label for="exampleText">
      address
    </Label>
    <Input
      id="exampleText"
      name="address"
      type="textarea"
      value = {formdata.address}
      onChange={handleInput}
    />
  </FormGroup>
  
  <FormGroup tag="fieldset">
    <legend>
      gender
    </legend>
    <FormGroup check>
      <Input
        name="radio1"
        type="radio" 
        value = "male"
        onChange={handleInput}
      />
      {' '}
      <Label check>
       male
      </Label>
    </FormGroup>
    <FormGroup check>
      <Input
        name="radio1"
        type="radio"
        value = "female"
        onChange={handleInput}
      />
      {' '}
      <Label check>
      female
      </Label>
    </FormGroup>
 
  </FormGroup>
  
  <Button type='submit'>
    registration
  </Button>
</Form>
    

        </ModalBody>
        {/* <ModalFooter>
          <Button color="primary" onClick={toggle}>
            Do Something
          </Button>{' '}
          <Button color="secondary" onClick={toggle}>
            Cancel
          </Button>
        </ModalFooter> */}
      </Modal>
    </div>
  );
}

export default Registration;
